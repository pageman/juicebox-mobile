package com.gotjuice.juicebox.menu;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gotjuice.juicebox.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by User on 8/8/15.
 */
public class MenuDrawerAdapter extends RecyclerView.Adapter<MenuDrawerAdapter.ConstantViewHolder> {

    @Override
    public ConstantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.menudrawerrow, parent, false);
        ConstantViewHolder holder = new ConstantViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ConstantViewHolder holder, int position) {
        MenuDrawerModel current = list.get(position);
        holder.title.setText(current.getTitle());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ConstantViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        public ConstantViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
        }
    }

    List<MenuDrawerModel> list = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    public MenuDrawerAdapter(Context context, List<MenuDrawerModel> list) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.list = list;
    }

    public void delete(int position) {

    }

}
