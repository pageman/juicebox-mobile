package com.gotjuice.juicebox.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.gotjuice.juicebox.StringsLOL;
import com.gotjuice.juicebox.corefiles.RecipeModel;
import com.gotjuice.juicebox.dialog.PopWait;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by User on 8/8/15.
 */
public class SendOrderTask extends AsyncTask<Integer, Integer, String>{

    ProgressDialog pd;
    Context context;
    int user;
    int recipeid;

    public SendOrderTask(Context context, int recipeid) {
        this.context = context;
        this.recipeid = recipeid;
    }


    @Override
    protected void onPreExecute() {
        this.pd = ProgressDialog.show(context, "", "Processing your order...", true);
    }

    @Override
    protected String doInBackground(Integer... integers) {

        String result = "";
        ArrayList<NameValuePair> postParameters;

        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(StringsLOL.SUBMIT_RECIPE);

            postParameters = new ArrayList<NameValuePair>();
            postParameters.add(new BasicNameValuePair("userID", Integer.toString(1))); //UserID
            postParameters.add(new BasicNameValuePair("recipeID", Integer.toString(recipeid)));

            httpPost.setEntity(new UrlEncodedFormEntity(postParameters));
            HttpResponse response = httpClient.execute(httpPost);
            result = EntityUtils.toString(response.getEntity());
        }
        catch (ClientProtocolException e1) { result = e1.getLocalizedMessage(); }
        catch (IOException e1) { result = e1.getLocalizedMessage(); }

        return result;
    }

    @Override
    protected void onPostExecute(String string) {
        Log.i("SendOrderTask.onPostExecute", "ping3: " + string);
        this.pd.dismiss();

        try {
            JSONObject jsonResponse = new JSONObject(string);
            JSONObject obj = jsonResponse.getJSONObject("result");
            new PopWait(context).Create(obj.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //bday, sex, height kg, weight cm
    }
}
