package com.gotjuice.juicebox.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gotjuice.juicebox.StringsLOL;
import com.gotjuice.juicebox.corefiles.RecipeAdapter;
import com.gotjuice.juicebox.corefiles.RecipeModel;
import com.gotjuice.juicebox.dialog.PopDialog;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 8/9/15.
 */
public class SendUserDataTask extends AsyncTask<Integer, String, String> {

    ProgressDialog pd;
    Context context;
    ImageView imageView;
    TextView name;
    TextView description;
    Button yes;
    Button no;
    String json;

    public SendUserDataTask(Context context, String json, ImageView imageView, TextView name, TextView description,
                            Button yes, Button no) {
        this.context = context;
        this.imageView = imageView;
        this.name = name;
        this.description = description;
        this.yes = yes;
        this.no = no;
        this.json = json;
    }

    @Override
    protected void onPreExecute() {
        this.pd = ProgressDialog.show(context, "", "Loading all the recipes...", true);
    }

    @Override
    protected String doInBackground(Integer... integers) {

        String result = "";
        ArrayList<NameValuePair> postParameters;

        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost("http://juicebox-v1.herokuapp.com/recipe/suggest");

            JSONObject obj = new JSONObject(json);
            JSONObject objX = obj.getJSONObject("result");

            String vitalStat = objX.getString("height") + "," + objX.getString("weight");
            String conditions = "";
            JSONArray cond = objX.getJSONArray("conditions");
            for (int i = 0; i < cond.length(); i++) {
                JSONObject conv = cond.getJSONObject(i);
                conditions += conv.getString("name") + ",";
            }

            postParameters = new ArrayList<NameValuePair>();
            postParameters.add(new BasicNameValuePair("data", vitalStat + "," + conditions));
            httpPost.setEntity(new UrlEncodedFormEntity(postParameters));

            HttpResponse response = httpClient.execute(httpPost);
            result = EntityUtils.toString(response.getEntity());
        }
        catch (ClientProtocolException e1) { result = e1.getLocalizedMessage(); }
        catch (IOException e1) { result = e1.getLocalizedMessage(); } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    protected void onPostExecute(String response) {
        this.pd.dismiss();
        Log.i("SendUserDataTask.onPostExecute", "ping1: " + json);
        Log.i("SendUserDataTask.onPostExecute", "ping1: " + response);
        int id = 0;
        try {
            JSONObject obj = new JSONObject(response);
            id = obj.getInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new GetSpecialRecipeTask(context, id, imageView, name, description, yes, no).execute(0);
    }
}
