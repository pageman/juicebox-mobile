package com.gotjuice.juicebox.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.gotjuice.juicebox.StringsLOL;
import com.gotjuice.juicebox.corefiles.RecipeAdapter;
import com.gotjuice.juicebox.corefiles.RecipeModel;
import com.gotjuice.juicebox.dialog.PopDialog;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by User on 8/8/15.
 */
public class GetAllRecipeTask extends AsyncTask<Integer, Integer, List<RecipeModel>>{

    ProgressDialog pd;

    Context context;
    ListView listview;
    RecipeAdapter adapter;

    List<RecipeModel> list;

    public GetAllRecipeTask(Context context, ListView listview, RecipeAdapter adapter) {
        this.context = context;
        this.listview = listview;
        this.adapter = adapter;
    }

    @Override
    protected void onPreExecute() {
        this.pd = ProgressDialog.show(context, "", "Loading all the recipes...", true);
        this.list = new ArrayList<RecipeModel>();
    }

    @Override
    protected List<RecipeModel> doInBackground(Integer... integers) {

        String result = "";
        List<RecipeModel> rm = new ArrayList<RecipeModel>();

        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(StringsLOL.LIST_ALL_RECIPES);
            HttpResponse response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            result = EntityUtils.toString(entity);

            JSONObject jsonResponse = new JSONObject(result);
            JSONArray jsonArray = jsonResponse.getJSONArray("result");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject j = jsonArray.getJSONObject(i);
                RecipeModel m = new RecipeModel();
                m.setId(j.getInt("id"));
                m.setImageUrl(j.getString("img_source"));
                m.setName(j.getString("name"));
                m.setDescription(j.getString("description"));
                rm.add(m);
            }
        }
        catch (JSONException e) { e.printStackTrace(); }
        catch (ClientProtocolException e1) { e1.printStackTrace(); }
        catch (IOException e1) { e1.printStackTrace(); }

        return rm;
    }

    @Override
    protected void onPostExecute(final List<RecipeModel> response) {
        this.pd.dismiss();
        this.list = response;

        adapter = new RecipeAdapter(context, response);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                RecipeModel m = response.get(i);

                new PopDialog(context).Create(m.getId(), m.getName());
            }
        });
    }
}
