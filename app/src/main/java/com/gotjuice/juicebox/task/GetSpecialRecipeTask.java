package com.gotjuice.juicebox.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gotjuice.juicebox.StringsLOL;
import com.gotjuice.juicebox.corefiles.RecipeAdapter;
import com.gotjuice.juicebox.corefiles.RecipeModel;
import com.gotjuice.juicebox.dialog.PopDialog;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 8/9/15.
 */
public class GetSpecialRecipeTask extends AsyncTask<Integer, Integer, String> {

    ProgressDialog pd;

    Context context;

    int id;

    ImageView imageView;
    TextView name;
    TextView description;
    Button yes;
    Button no;

    List<RecipeModel> list;

    public GetSpecialRecipeTask(Context context, int id, ImageView imageView, TextView name, TextView description,
                            Button yes, Button no) {
        this.context = context;
        this.id = id;
        this.imageView = imageView;
        this.name = name;
        this.description = description;
        this.yes = yes;
        this.no = no;
    }

    @Override
    protected void onPreExecute() {
        this.pd = ProgressDialog.show(context, "", "Loading all the recipes...", true);
        this.list = new ArrayList<RecipeModel>();
    }

    @Override
    protected String doInBackground(Integer... integers) {

        String result = "";
        List<RecipeModel> rm = new ArrayList<RecipeModel>();
        ArrayList<NameValuePair> postParameters;

        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(StringsLOL.SUGGESTED);

            postParameters = new ArrayList<NameValuePair>();
            postParameters.add(new BasicNameValuePair("id", Integer.toString(id)));

            httpPost.setEntity(new UrlEncodedFormEntity(postParameters));
            HttpResponse response = httpClient.execute(httpPost);
            result = EntityUtils.toString(response.getEntity());

        }
        catch (ClientProtocolException e1) { result = e1.getLocalizedMessage(); }
        catch (IOException e1) { result = e1.getLocalizedMessage(); }

        return result;
    }

    @Override
    protected void onPostExecute(String response) {
        this.pd.dismiss();
        Log.i("GetSpecialRecipeTask.onPostExecute", "ping2: " + response);
        try {
            JSONObject jsonResponse = new JSONObject(response);
            JSONObject obj = jsonResponse.getJSONObject("result");

            final String _id = obj.getString("id");
            Picasso.with(context).load(obj.getString("img_source")).into(imageView);
            name.setText(obj.getString("name"));
            description.setText(obj.getString("description"));

            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new SendOrderTask(context, Integer.parseInt(_id)).execute(0);
                }
            });

            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }



    }
}
