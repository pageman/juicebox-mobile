package com.gotjuice.juicebox.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gotjuice.juicebox.StringsLOL;
import com.gotjuice.juicebox.session.SessionManagement;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by User on 8/9/15.
 */
public class LoginTask extends AsyncTask<Integer, String, String> {


    ProgressDialog pd;
    Context context;
    ImageView imageView;
    TextView name;
    TextView description;
    Button yes;
    Button no;

    SessionManagement session;

    public LoginTask(Context context, ImageView imageView, TextView name, TextView description,
                            Button yes, Button no) {
        this.context = context;
        this.imageView = imageView;
        this.name = name;
        this.description = description;
        this.yes = yes;
        this.no = no;
    }

    @Override
    protected void onPreExecute() {
        this.pd = ProgressDialog.show(context, "", "Logging in...", true);
        session = new SessionManagement(context);
    }

    @Override
    protected String doInBackground(Integer... integers) {

        String result = "";
        ArrayList<NameValuePair> postParameters;

        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(StringsLOL.USER);

            postParameters = new ArrayList<NameValuePair>();
            postParameters.add(new BasicNameValuePair("id", Integer.toString(1)));

            httpPost.setEntity(new UrlEncodedFormEntity(postParameters));
            HttpResponse response = httpClient.execute(httpPost);
            result = EntityUtils.toString(response.getEntity());
        }
        catch (ClientProtocolException e1) { result = e1.getLocalizedMessage(); }
        catch (IOException e1) { result = e1.getLocalizedMessage(); }

        return result;
    }

    @Override
    protected void onPostExecute(String response) {
        this.pd.dismiss();
        Log.i("LoginTask.onPostExecute", "ping0: " + response);
        session.createLoginSession(response);
        new SendUserDataTask(context, response, imageView, name, description, yes, no).execute(0);

    }
}
