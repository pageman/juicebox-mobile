package com.gotjuice.juicebox;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.gotjuice.juicebox.fragment.MenuFragment;

/**
 * Created by User on 8/8/15.
 */
public class UserProfile extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView textview;
    private MenuFragment menudrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.userprofile);

        toolbar = (Toolbar) findViewById(R.id.toolbar2);
        textview = (TextView) findViewById(R.id.title);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");

        textview.setText(getResources().getString(R.string.userprofile).toUpperCase());
        Typeface font = Typeface.createFromAsset(getAssets(),  "fonts/Raleway-ExtraBold.ttf");
        textview.setTypeface(font);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
