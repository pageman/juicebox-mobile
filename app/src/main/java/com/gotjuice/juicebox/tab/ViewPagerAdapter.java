package com.gotjuice.juicebox.tab;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.widget.Toolbar;

import com.gotjuice.juicebox.R;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    CharSequence Titles[];
    int NumbOfTabs;
    TabToday today;
    TabRecipes recipes;
    Toolbar toolbar;
    Context context;

    public ViewPagerAdapter(FragmentManager fm,CharSequence mTitles[], int mNumbOfTabsumb, Toolbar toolbar, Context context) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;
        this.toolbar = toolbar;
        this.context = context;

        today = new TabToday();
        recipes = new TabRecipes();
    }

    @Override
    public Fragment getItem(int position) {

        TabToday today = new TabToday();
        TabRecipes recipes = new TabRecipes();

        switch (position) {
            case 0:
                toolbar.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_oranges));
                return today;
            default:
                toolbar.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.bg_oranges));
                return recipes;
        }
        //return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}
