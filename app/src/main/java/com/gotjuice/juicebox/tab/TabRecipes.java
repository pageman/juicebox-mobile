package com.gotjuice.juicebox.tab;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gotjuice.juicebox.R;
import com.gotjuice.juicebox.corefiles.RecipeAdapter;
import com.gotjuice.juicebox.corefiles.RecipeModel;
import com.gotjuice.juicebox.task.GetAllRecipeTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

/**
 * Created by User on 8/8/15.
 */
public class TabRecipes extends Fragment {

    public View view;

    ListView list;
    RecipeAdapter adapter;

    GetAllRecipeTask task;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Initialize();
        task = new GetAllRecipeTask(getActivity(), list, adapter);
        task.execute(0);
    }

    @Override
    public void onResume() {
        super.onResume();
        //this.getActivity().findViewById(R.id.toolbar).setBackgroundResource(R.drawable.bg_leaves);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TabRecipes.class.getSimpleName(), "TabRecipes.onCreateView");
        view = inflater.inflate(R.layout.tabrecipes, container, false);
        return view;
    }

    void Initialize() {
        list = (ListView) view.findViewById(R.id.recipelist);
    }
}
