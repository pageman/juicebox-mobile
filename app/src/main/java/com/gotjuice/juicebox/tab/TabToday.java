package com.gotjuice.juicebox.tab;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gotjuice.juicebox.R;
import com.gotjuice.juicebox.session.SessionManagement;
import com.gotjuice.juicebox.task.LoginTask;
import com.gotjuice.juicebox.task.SendUserDataTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by User on 8/8/15.
 */
public class TabToday extends Fragment{

    public View view;
    ImageView image;
    TextView name;
    TextView description;
    Button yes;
    Button no;

    SendUserDataTask sendusertask;
    LoginTask logintask;
    SessionManagement session;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Initialize();


        if(session.isLoggedIn()) {
            HashMap<String, String> data = session.getUserDetails();
            String userdata = data.get("user");

            sendusertask = new SendUserDataTask(getActivity(), userdata, image, name, description, yes, no);
            sendusertask.execute(0);
        } else {
            logintask = new LoginTask(getActivity(), image, name, description, yes, no);
            logintask.execute(0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TabToday.class.getSimpleName(), "TabToday.onCreateView");
        view = inflater.inflate(R.layout.tabtoday, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        //this.getActivity().findViewById(R.id.toolbar).setBackgroundResource(R.drawable.bg_oranges);
    }

    void Initialize() {
        image = (ImageView) view.findViewById(R.id.tabtoday_image);
        name = (TextView) view.findViewById(R.id.tabtoday_name);
        description = (TextView) view.findViewById(R.id.tabtoday_details);
        yes = (Button) view.findViewById(R.id.tabtoday_makethisdrink);
        no = (Button) view.findViewById(R.id.tabtoday_chooseanother);
        session = new SessionManagement(getActivity());
    }
}
