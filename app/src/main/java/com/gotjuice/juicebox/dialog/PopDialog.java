package com.gotjuice.juicebox.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.gotjuice.juicebox.R;
import com.gotjuice.juicebox.task.SendOrderTask;

/**
 * Created by User on 8/8/15.
 */
public class PopDialog {

    Context context;
    TextView text;
    Button no;
    Button yes;

    public PopDialog(Context context) {
        this.context = context;
    }

    public PopDialog Create(int id, String name) {

        final int _id = id;

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popdialog);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        text = (TextView) dialog.findViewById(R.id.popdialog_text);
        no = (Button) dialog.findViewById(R.id.popdialog_no);
        yes = (Button) dialog.findViewById(R.id.popdialog_yes);

        text.setText("Are you sure you want the " + name + "?");
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { dialog.dismiss(); }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                new SendOrderTask(context, _id).execute(0);
            }
        });

        dialog.show();

        return this;
    }

}
