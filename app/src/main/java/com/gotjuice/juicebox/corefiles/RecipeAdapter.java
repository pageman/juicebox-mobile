package com.gotjuice.juicebox.corefiles;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gotjuice.juicebox.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by User on 8/8/15.
 */
public class RecipeAdapter extends BaseAdapter {

    List<RecipeModel> model;
    Context context;

    private class ViewHolder {
        ImageView image;
        TextView name;
        TextView description;
    }

    public RecipeAdapter(Context context, List<RecipeModel> model) {
        this.model = model;
        this.context = context;
    }

    @Override
    public int getCount() { return model.size(); }

    @Override
    public Object getItem(int i) { return model.get(i); }

    @Override
    public long getItemId(int i) { return i; }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        RecipeModel m = model.get(position);
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.tabrecipes_row, parent, false);
            holder.image = (ImageView) convertView.findViewById(R.id.tabrecipes_row_image);
            holder.name = (TextView) convertView.findViewById(R.id.tabrecipes_row_name);
            holder.description = (TextView) convertView.findViewById(R.id.tabrecipes_row_detail);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Picasso.with(context).load(m.getImageUrl()).into(holder.image);
        holder.name.setText(m.getName());
        holder.description.setText(m.getDescription());

        return convertView;
    }
}
