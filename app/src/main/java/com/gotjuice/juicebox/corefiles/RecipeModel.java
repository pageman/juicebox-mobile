package com.gotjuice.juicebox.corefiles;

/**
 * Created by User on 8/8/15.
 */
public class RecipeModel {

    private int id;
    private String imageUrl;
    private String name;
    private String description;

    public int getId() { return id; }

    public void setId(int id) { this.id = id;}

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
