package com.gotjuice.juicebox.session;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by User on 8/9/15.
 */
public class SessionManagement {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "jb_con";
    private static final String IS_LOGIN = "state";
    public static final String KEY_USER = "user";

    public SessionManagement(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String user){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USER, user);
        editor.commit();
    }

    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_USER, pref.getString(KEY_USER, null));
        return user;
    }

    public void checkLogin() {
        if(!this.isLoggedIn()){

        } else {

        }
    }

    public void logoutUser() {
        editor.clear();
        editor.commit();
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}
